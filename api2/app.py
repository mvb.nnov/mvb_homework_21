from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import requests

URL = 'http://less21_hw_api_3:5003'
# URL = 'http://127.0.0.1:5003'
# URL = 'http://0.0.0.0:5003'
app = Flask(__name__)

@app.route("/")
def home():
    try:
        response = requests.get(URL, verify=False)
        if response.status_code != 200:
            return (f"2:error-200")
        else:
            return (f"2:{response.text}")
    except Exception as ex:
        return (f"api-2: exception \n{ex.args[0]}")

# дальше идут всякие переборы вариантов обращения к апи-3

@app.route("/a/")
def home2():
    try:
        response = requests.get('http://less21_hw_api_3', verify=False)
        if response.status_code != 200:
            return (f"2:error-200")
        else:
            return (f"2:{response.text}")
    except Exception as ex:
        return (f"api-2: exception \n{ex.args[0]}")

@app.route("/b/")
def home3():
    try:
        response = requests.get('http://less21_hw_api_3:5000', verify=False)
        if response.status_code != 200:
            return (f"2:error-200")
        else:
            return (f"2:{response.text}")
    except Exception as ex:
        return (f"api-2: exception \n{ex.args[0]}")

@app.route("/c/")
def home4():
    try:
        response = requests.get('http://less21_hw_api_3:5003', verify=False)
        if response.status_code != 200:
            return (f"2:error-200")
        else:
            return (f"2:{response.text}")
    except Exception as ex:
        return (f"api-2: exception \n{ex.args[0]}")

@app.route("/test/")
def route_test():
    return (f"api-2: test")

if __name__ == "__main__":
    app.run()