from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import requests

# URL = 'http://127.0.0.1:5002'
URL = 'http://less21_hw_api_2:5002'

app = Flask(__name__)

@app.route("/")
def home():
    try:
        response = requests.get(URL)
        if response.status_code != 200:
            return (f"api-1: error-200 ")
        else:
            return (f"api-1: {response.text}")
    except Exception as ex:
        return (f"api-1: exception {ex.args[0]}")

if __name__ == "__main__":
    app.run()